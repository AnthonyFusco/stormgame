(ns stormgame.handler
  (:require
    [ring.middleware.cors :refer [wrap-cors]]
    [ring.middleware.json :refer [wrap-json-response wrap-json-body]]
    [ring.middleware.params :refer [wrap-params]]
    [compojure.core :refer [GET POST defroutes]]
    [compojure.route :refer [resources not-found]]
    [ring.util.response :refer [resource-response response]]
    [ring.middleware.reload :refer [wrap-reload]]
    [shadow.http.push-state :as push-state]))

(def lol "yeah lol")
(def db-init {
              :room1 {
                      :player1 {
                                :name "toto"
                                }
                      :player2 {
                                :name "lolol"
                                }
                      }
              :room2 {
                      :player1 {
                                :name "toto2"
                                }
                      }
              })
(def db (atom db-init))

(defn get-player-data
  ([db room player] (player (room db) {}))
  ([db room] (room db {})))

(defn get-player-actions [db room player]
  (:actions (player (room db)) []))

(defn queue-action [db room player new-action]
  (let [old (get-player-actions db room player)]
    (assoc-in db [room player :actions] (into old [new-action]))))

(defroutes routes
           (GET "/" [] (resource-response "index.html" {:root "public"}))
           ;(GET "/api/lol" [] lol)
           (GET "/:room/:player" [room player]
             (response (get-player-data @db (keyword room) (keyword player))))
           (GET "/:room" [room]
             (response (get-player-data @db (keyword room))))
           (POST "/action/:room/:player" {{player :player
                                           room   :room} :params
                                          body           :body}
             (reset! db (queue-action @db (keyword room) (keyword player) (:action body)))
             (response @db))
           (not-found "<h1>Page not found</h1>"))

(def dev-handler (-> #'routes wrap-reload push-state/handle))

(def handler
  (-> routes
      (wrap-cors :access-control-allow-origin [#"http://localhost:8280"]
                 :access-control-allow-methods [:get :put :post :delete])
      wrap-json-response
      (wrap-json-body {:keywords? true})
      wrap-params
      ))
