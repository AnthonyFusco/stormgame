(ns stormgame.views
  (:require
    [re-frame.core :as re-frame]
    [stormgame.subs :as subs]
    [stormgame.events :as events]
    [stormgame.utils :as utils]
    ))

(defn login-page []
  (let [player (re-frame/subscribe [::subs/form-player-name])
        room (re-frame/subscribe [::subs/form-room-name])
        player-data (re-frame/subscribe [::subs/test])]
    (fn []
      [:div
       [:div
        [:label "Room id"]
        [:input {:type "text" :on-change #(re-frame/dispatch [::events/login-form [:room (-> % .-target .-value)]])}]]
       [:div
        [:label "Player name"]
        [:input {:type "text" :on-change #(re-frame/dispatch [::events/login-form [:player (-> % .-target .-value)]])}]]
       [:div @player]
       [:div @room]
       [:input {:type     "button" :value "Click me!"
                :on-click #(re-frame/dispatch [::events/get-player-data @room @player])}]
       [:div @player-data]
       ])))

(defn home []
  (fn [] [:div "Home"]))

(def routes
  [
   {:key       :home
    :component home
    :title     "Home"
    }
   {:key       :login
    :component login-page
    :title     "Login"
    }
   ])

(defn tab-selector [routes]
  (let [active (re-frame/subscribe [::subs/active-panel])
        find-panel (fn [panels key] (utils/find-first (fn [elt] (= (:key elt) key)) panels))
        title-active (:title (find-panel routes @active))]
    [:ul
     (for [{:keys [title key]} routes]
       [:li {:key      key
             :on-click #(re-frame/dispatch [::events/set-active-panel key])
             :style    {:cursor "pointer"
                        :color  (if (= title title-active) "red" "black")}}
        title])]))

(defn pages [page-name]
  (case page-name
    :home [home]
    :login [login-page]
    [home]))

(defn main-panel []
  (let [active @(re-frame/subscribe [::subs/active-panel])]
    [:div
     [tab-selector routes]
     [pages active]
     ]))
