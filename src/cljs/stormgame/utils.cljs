(ns stormgame.utils)

(defn find-first
  [f coll]
  (first (filter f coll)))
