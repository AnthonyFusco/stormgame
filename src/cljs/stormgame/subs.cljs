(ns stormgame.subs
  (:require
   [re-frame.core :as re-frame]))

(re-frame/reg-sub
 ::name
 (fn [db]
   (:name db)))

(re-frame/reg-sub
  ::test
  (fn [db]
    (:test db)))

(re-frame/reg-sub
  ::form-player-name
  (fn [db]
    (:player (:login-form db))))

(re-frame/reg-sub
  ::form-room-name
  (fn [db]
    (:room (:login-form db))))

(re-frame/reg-sub
  ::active-panel
  (fn [db]
    (:active-panel db)))

