(ns stormgame.events
  (:require
    [re-frame.core :as re-frame]
    [stormgame.db :as db]
    [ajax.core :as ajax]
    [clojure.string :as str]))

(def api-url "http://localhost:3000")

(defn endpoint
  "Concat any params to api-url separated by /"
  [& params]
  (str/join "/" (concat [api-url] params)))

(re-frame/reg-event-db
  ::initialize-db
  (fn [_ _]
    db/default-db))

(re-frame/reg-event-db
  ::login-form
  (fn [db [_ [k v]]]
    (assoc-in db [:login-form k] v)))

(re-frame/reg-event-fx
  ::get-player-data
  (fn [_ [_ room player]]
    {:http-xhrio {:method          :get
                  :uri             (endpoint room player)
                  :timeout         8000
                  :response-format (ajax/raw-response-format)
                  :on-success      [::success-http-result]
                  :on-failure      [::bad-http-result]}}))

(re-frame/reg-event-db
  ::success-http-result
  (fn [db [_ result]]
    (assoc db :test result)))

(re-frame/reg-event-db
  ::bad-http-result
  ;[re-frame.core/debug]
  (fn [db [_ result]]
    (assoc db :test result)))

(re-frame/reg-event-db
  ::set-active-panel
  (fn [db [_ value]]
    (assoc db :active-panel value)))
