(ns stormgame.core
  (:require
   [reagent.dom :as rdom]
   [re-frame.core :as re-frame]
   [stormgame.events :as events]
   [stormgame.views :as views]
   [stormgame.config :as config]
   [day8.re-frame.http-fx]
   ))


(defn dev-setup []
  (when config/debug?
    (println "dev mode")))

(defn ^:dev/after-load mount-root []
  (re-frame/clear-subscription-cache!)
  (let [root-el (.getElementById js/document "app")]
    (rdom/unmount-component-at-node root-el)
    (rdom/render [views/main-panel] root-el)))

(defn init []
  (re-frame/dispatch-sync [::events/initialize-db])
  (re-frame/dispatch-sync [::events/set-active-panel :panel1])
  (dev-setup)
  (mount-root))
